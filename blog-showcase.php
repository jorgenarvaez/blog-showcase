<?php
/*
* Plugin Name: Blog Showcase
* Description: Blog showcase grid by JNM
* Version: 0.0.1
* Author: Jorge Narváez de la Mora
* Author URI: https://narvaezdelamora.com
*/

function scripts_showcase() {
    $plugin_url = plugin_dir_url( __FILE__ );

    wp_enqueue_style( 'showcase-css', $plugin_url . 'css/styleshowcase.css' );
    wp_enqueue_script('showcase-js', $plugin_url . 'js/scriptshowcase.js', true );
}
add_action( 'wp_enqueue_scripts', 'scripts_showcase' );



function postshowcase( $pstatts ) {
	ob_start();

	// Defaults
	extract( shortcode_atts( array (
		'type' => 'post',
		'order' => 'date',
		'orderby' => 'title',
		'pperpage' => 5,
		'category' => '',
	), $pstatts ) );

	$pstoptions = array(
		'post_type' => $type,
		'order' => $order,
		'orderby' => $orderby,
		'posts_per_page' => $pperpage,
		'category_name' => $category,
	);
	// Define our WP Query Parameters
	$the_query = new WP_Query( $pstoptions ); 

	// Start our WP Query
	while ($the_query -> have_posts()) : $the_query -> the_post();

		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
	?>
	<div class="post-wrapper">
		<a class="linkbox" href="<?php the_permalink() ?>" style="background-image:  url( <?php echo $thumb_url[0]; ?> ); ">
			<div class="datebox">
				<h2><?php echo get_the_date('j'); ?></h2>
				<p><?php echo get_the_date('M, Y'); ?></p>
			</div>
			
			<div class="titlebox">
				<p><?php the_title(); ?></p>
			</div>
		</a>
	</div>
	<?php
	// Repeat the process and reset once it hits the limit
	endwhile;
	wp_reset_postdata();

	$output = ob_get_clean();

	return $output;
	
}
add_shortcode('showcase-blog', 'postshowcase');

?>